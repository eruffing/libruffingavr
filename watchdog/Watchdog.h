/**
 * \file Watchdog.h
 *
 * \since 2015-11-13
 * \author Ethan Ruffing
 *
 * \brief Tools for implementing a watchdog timer.
 *
 * This file contains tools for setting up and controlling a watchdog timer.
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>

#ifndef WATCHDOG_H_
#define WATCHDOG_H_

/**
 * Sets up the watchdog timer for an 8 s timeout in System Reset mode. To
 * prevent the watchdog timer from timing gout, be sure to call `wdt_reset()`
 * regularly.
 */
void initWatchdog();

#endif /* WATCHDOG_H_ */
