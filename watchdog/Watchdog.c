/**
 * \file Watchdog.c
 *
 * \since 2015-11-13
 * \author Ethan Ruffing
 *
 * \brief Implementation of Watchdog.h
 *
 * This file contains implementations of the functions declared in Watchdog.h.
 */ 

#include "Watchdog.h"

void initWatchdog() {
	cli();
	wdt_reset();
	WDTCSR |= _BV(WDCE) | _BV(WDE);
	WDTCSR = _BV(WDE) | _BV(WDP3) | _BV(WDP0);
	sei();
}
