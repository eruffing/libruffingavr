/**
 * \addtogroup sensors
 * \{
 * \file MotionDetector.h
 *
 * \since 2015-11-24
 * \author Ethan Ruffing
 * 
 * \brief Motion detector driver
 *
 * This file provides tools for using the PIR motion sensor.
 */ 

#include <avr/io.h>
#include "../preprocessorutils/AVRPreprocessorUtils.h"

#ifndef MOTIONDETECTOR_H_
#define MOTIONDETECTOR_H_

/**
 * Initializes the motion detector.
 *
 * \param      ddr The data direction register to which the motion detector is
 *             is connected.
 * \param      bit The bit within the register to which the motion detector is
 *             connected.
 */
#define initMotionDetector(ddr, bit) ({\
	DDR(ddr) &= ~_BV(bit);\
	PCICR |= _BV(PCIE1);\
	PCMSK1 |= _BV(bit);\
})

/**
 * Determines if the motion detector is currently outputting a HIGH.
 *
 * \param      ddr The data direction register to which the motion detector is
 *             is connected.
 * \param      bit The bit within the register to which the motion detector is
 *             connected.
 *
 * \return 1 if motion is detected, 0 if not.
 */
#define motionDetected(ddr, bit) (!(!(PIN(ddr) & _BV(bit))))

#endif /* MOTIONDETECTOR_H_ */

/**
 * \}
 */
