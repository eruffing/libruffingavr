/**
 * \file RGB_LED.h
 *
 * \since 2015-09-11
 * \author Ethan Ruffing
 *
 * \brief Definition for interface with an RGB LED.
 *
 * @details
 *     Contains definitions of functions for interfacing with an RGB LED.
 *     Assumes that the LED is connected with all cathodes on separate pins.
 *     (These are specified by the corresponding preprocessor directives.)
 */ 

#include <avr/io.h>
#include "config.h"

#ifndef RGB_LED_H_
#define RGB_LED_H_

/**
 * Sets up the outputs for using the LED.
 */
void initRgbLed();

/**
 * Enables the specified light in the RGB LED.
 * 
 * \param[in] c
 *     The color to enable - 'r' for red, 'g' for green, and 'b' for blue.
 */
void rgbOn(char c);

/**
 * Disables the specified light in the RGB LED.
 * 
 * \param[in] c
 *     The color to disable - 'r' for red, 'g' for green, and 'b' for blue.
 */
void rgbOff(char c);

/**
 * Enables the specified color in the RGB LED and disables all others.
 *
 * \param[in] c
 *     The color to enable - 'r' for red, 'g' for green, and 'b' for blue.
 */
void rgbChange(char c);

/**
 * Enables the RED LED and informs the user that the it was chosen.
 */
void rgbClear();


#endif /* RGB_LED_H_ */
