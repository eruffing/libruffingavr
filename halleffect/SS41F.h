/**
 * \addtogroup sensors Sensors
 * \brief Environmental sensors
 * \{
 * \file SS41F.h
 *
 * \since 2015-11-25
 * \author Ethan Ruffing
 *
 * \brief Hall Effect sensor drivers
 *
 * This file provides drivers for the Honeywell SS41F Hall Effect Sensor.
 */ 

#include <avr/io.h>
#include "../preprocessorutils/AVRPreprocessorUtils.h"

#ifndef SS41F_H_
#define SS41F_H_

/**
 * Initializes a Hall Effect Sensor.
 * 
 * \param ddr The letter of the register to which the hall effect sensor is
 *     connected.
 * \param bit The bit within the register to which the hall effect sensor is
 *     connected.
 */
#define initHallEffect(ddr, bit) ({\
 	DDR(ddr) &= ~(_BV(bit));\
 	PORT(ddr) |= _BV(bit);\
 	PCICR |= _BV(PCIF2);\
 	PCMSK2 |= _BV(bit);\
 })

/**
 * Determines if the Hall Effect sensor is in the latched state.
 *
 * \param ddr The letter of the register to which the hall effect sensor is
 *     connected.
 * \param bit The bit within the register to which the hall effect sensor is
 *     connected.
 * 
 * \returns
 *     1 if the sensor is in the latched state, 0 if not.
 */
#define hallEffectLatched(ddr, bit) (!(!(PIN(ddr) & _BV(bit))))

#endif /* SS41F_H_ */

/**
 * \}
 */
