/**
 * \addtogroup input \Input
 * \brief User input
 * \details Tools for managing user input.
 * \{
 * \file Keypad.h
 *
 * \since 2015-09-25
 * \author Ethan Ruffing
 *
 * \brief Interface with a 3x4 numeric keypad
 *
 * This file contains functions for setting up and using a 3x4 numeric keypad.
 *
 * Note that the current state of this program assumes the keypad to be the only
 * thing connected to a PCF8574 I/O expander and that it is the only expander
 * present.
 *     
 * To use, call `getKey()` to get the character of the currently pressed key.
 * Alternatively, use `getInput()` to wait for a key to be pressed and released,
 * then receive that key's character.
 */

#ifndef F_CPU
#error F_CPU must be defined before including Keypad.h!
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include "../ioexpander/PCF8574.h"

#ifndef KEYPAD_H_
#define KEYPAD_H_

static const unsigned char PROGMEM keypad[4][3] = {{'1', '2', '3'},
                                                   {'4', '5', '6'},
                                                   {'7', '8', '9'},
                                                   {'*', '0', '#'}};

/**
 * Initializes the IO expander to the proper state.
 */
void initKeypad();

/**
 * Gets the key that is pressed on the keypad.
 *
 * This function is based on a program by "Tushar" found at
 * http://www.embedds.com/interfacing-matrix-keyboard-with-avr/
 * 
 * \return
 *     Returns the character of the key that is pressed, or '\0' if no key is
 *     currently pressed.
 */
char getKey();

/**
 * Wait until a key has been pressed and released, then return the character
 * of that key.
 * 
 * \return
 *     The character corresponding to the key that was pressed.
 */
char getInput();

/**
 * Provides a mapping between the row and column indeces and the characters
 * represented on the keypad.
 * 
 * \param[in] row
 *     The index of the key's row.
 * \param[in] column
 *     The index of the key's column.
 * \return
 *     The character corresponding to the specified indeces, or '\0' if no
 *     corresponding character exists.
 */
char indexToChar(int row, int column);

#endif /* KEYPAD_H_ */

/**
 * \}
 */
