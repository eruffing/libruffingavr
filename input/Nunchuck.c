/**
 * @file Nunchuck.c
 * @since 2015-01-13
 * @author Ethan Ruffing
 * @brief Implementation of Nunchuck.h
 *
 * This file contains implementations of the functions declared in Nunchuck.h
 */

#include "Nunchuck.h"

void initNunchuck(NunchuckColor nc) {
	twiSendStart();
	twiSendAddress(WIINUNCHUCK_ADDR);
    if (nc == white) {
        twiSendByte(0x40);
        twiSendByte(0x00);
    } else {
        twiSendByte(0xF0);
        twiSendByte(0x55);
        twiSendByte(0xFB);
        twiSendByte(0x00);
    }
    twiSendStop();
}

void nunchuckReadSensors() {
    twiSendStart();
    twiSendAddress(WIINUNCHUCK_ADDR);
    twiSendByte(0x00);
    twiSendStop();
}

NunchuckData nunchuckGetData() {
    uint8_t buf[6], i;
    
    twiSendStart();
    twiSendAddress(WIINUNCHUCK_ADDR | 0x01);
    
    for (i = 0; i < 5; i++) {
        buf(i) = twiReadACK();
    }
    buf(5) = twiReadNACK();
    
    twiSendStop();
    
    //TODO: add stuff to convert the received data to useable info
}

NunchuckData nunchuckRead() {
    nunchuckReadSensors();
    return nunchuckGetData();
}
