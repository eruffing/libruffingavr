/**
 * @file Nunchuck.h
 * @since 2015-01-13
 * @author Ethan Ruffing
 * @brief Wii Nunchuck driver
 *
 * This file provides drivers for using a Wii Nunchuck.
 */

#include <avr/io.h>
#include "../communication/TWI.h"

#ifndef NUNCHUCK_H_
#define NUNCHUCK_H_

/**
 * The TWI address of the Wii Nunchuck.
 */
#define WIINUNCHUCK_ADDR (0xA4)

/**
 * A variety of nunchuck.
 */
enum NunchuckColor {black, white};

/**
 * All data returned from the nunchuck.
 */
typedef struct NunchuckData {
    /** The x-axis of the joystick. */
    uint8_t joyX;
    /** The y-axis of the joystick. */
    uint8_t joyY;
    /** The x-axis of the accelerometer (10-bit resolution). */
    uint16_t accelX;
    /** The y-axis of the accelerometer (10-bit resolution). */
    uint16_t accelY;
    /** The z-axis of the accelerometer (10-bit resolution). */
    uint16_t accelZ;
    /** Whether the C button is pressed. */
    uint8_t buttonC;
    /** Whether the Z button is pressed. */
    uint8_t buttonZ;
} NunchuckData;

/**
 * Initializes the Wii Nunchuck.
 *
 * @param[in] nc
 *     The type of nunchuck to initialize.
 */
void initNunchuck(NunchuckColor nc);

/**
 * Sends the command for the nunchuck to read from its sensors.
 */
void nunchuckReadSensors();

/**
 * Reads the most recent data from the nunchuck.
 * 
 * @returns The data read from the nunchuck.
 */
NunchuckData nunchuckGetData();

/**
 * Sends the command for the nunchuck to read from its sensors, then reads that
 * data from the nunchuck.
 *
 * @returns The data read from the nunchuck.
 */
NunchuckData nunchuckRead();

#endif /* NUNCHUCK_H_ */
