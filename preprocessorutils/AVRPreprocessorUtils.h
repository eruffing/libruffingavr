/**
 * \file AVRPreprocessorUtils.h
 * 
 * \author Ethan Ruffing
 * \since 2015-09-26
 * 
 * \brief Tools for manipulating the AVR preprocessor library
 * 
 * This file contains several tools for more clever/advanced use of the macros
 * provided by the AVR library.
 *
 * These are based on those provided in a program by "Tushar" found at
 * http://www.embedds.com/interfacing-matrix-keyboard-with-avr/
 */

#include "PreprocessorStringUtils.h"

#ifndef AVRPREPROCESSORUTILS_H_
#define AVRPREPROCESSORUTILS_H_

/**
 * @def DDR(x)
 *     Will expand to the AVR macro "DDRA", "DDRB", "DDRC", etc. in the form
 *     "DDRX".
 */
#define DDR(x) _CONCAT(DDR,x)

/**
 * @def PORT(x)
 *     Will expand to the AVR macro "PORTA", "PORTB", "PORTC", etc. in the form
 *     "PORTX".
 */
#define PORT(x) _CONCAT(PORT,x)

/**
 * @def PIN(X)
 *     Will expand to the AVR macro "PINA", "PINB", "PINC", etc. in the form
 *     "PINX".
 */
#define PIN(x) _CONCAT(PIN,x)

#endif
