AVRLibraries
============
Copyright 2015-2016 by Ethan Ruffing. All rights reserved.

This is a set of libraries I've created over the use for using various
peripherals with the AVR development environment, especially on the ATmega328P
microcontroller.

Documentation
-------------
Complete program documentation can be generated using
[doxygen](http://www.stack.nl/~dimitri/doxygen/) by simply executing the
`doxygen` command in the repository's root directory. The resulting output will
be placed in a `doc` subdirectory.
