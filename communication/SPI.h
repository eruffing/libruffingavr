/**
 * \addtogroup communications Communications
 * \brief Tools for communicating with peripheral devices
 * \{
 * \file SPI.h
 *
 * \since 2015-10-09
 * \author Ethan Ruffing
 *
 * \brief SPI driver for the ATmega328P
 *
 * This file contains drivers for using the ATmega328P's SPI functionality.
 *
 * Note that use of the built-in SPI functionality requires the use of PB2, PB3,
 * PB4, and PB5 for SS, MOSI, MISO, and SCK, respectively.
 *
 * This program is adapted from that included with the AVR Application Note 151:
 * SPI on AVR, as well as from code by Michael Spiceland found at
 * https://github.com/mspiceland/avr-spiceduino-3310-thermistor.
 */ 

#include <avr/io.h>

#ifndef SPI_H_
#define SPI_H_

/**
 * Initializes the SPI system.
 */
void initSPIMaster();

/**
 * Sends a character.
 * 
 * \param[in] c
 *     The character to send.
 */
void spiSendChar(char c);


#endif /* SPI_H_ */

/**
 * \}
 */
