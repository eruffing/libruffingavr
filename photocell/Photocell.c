/**
 * \file Photocell.c
 *
 * \since 2015-09-18
 * \author Ethan Ruffing
 * 
 * \brief Implementation of Photocell.h
 *
 * This file contains implementations of the functions declared in Photocell.h.
 */

#include "Photocell.h"

/**
 * @var refVoltage
 *     The voltage at which the microcontroller is running (AVcc).
 */
static float refVoltage = 5.0;

void initPhotoCell(float refLevel) {
	refVoltage = refLevel;
	
	// Turn on the ADC and set prescaler to divide-by-128
	ADCSRA = 0;
	ADCSRA = _BV(ADEN) | _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);
	
	// Set the reference voltage to AVcc
	ADMUX |= _BV(REFS0);
	ADMUX &= ~_BV(REFS1);
	
	ADMUX |= PHOTOCELL_BIT;	// Set the ADC multiplexer selection
	
	ADMUX &= ~_BV(ADLAR);	// Set for 10-bit conversion
}

float readVoltage() {
	ADCSRA |= _BV(ADIF);	// Clear the ADC interrupt flag.
	ADCSRA |= _BV(ADSC);	// Start the next conversion
	while (!(ADCSRA & _BV(ADIF)));	// Wait for conversion to complete.
	int adcval = ADC;
	return (float)adcval / 1024.0 * refVoltage;	// Return calculated voltage.
}
