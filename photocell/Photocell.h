/**
 * \addtogroup sensors
 * \{
 * \file Photocell.h
 *
 * \since 2015-09-18
 * \author Ethan Ruffing
 *
 * \brief Use the ADC for reading a photocell at half-second intervals
 * 
 * This file contains definitions for functions to use in managing the
 * analog-to-digital converter for use in reading a photocell at half-second
 * intervals.
 */

#include <avr/io.h>
#include "config.h"

#ifndef PHOTOCELL_H_
#define PHOTOCELL_H_

/**
 * Sets up the photocell for use on the appropriate pin.
 *
 * \param[in] refLevel
 *     The reference voltage level at which the board is operating.
 */
void initPhotoCell(float refLevel);

/**
 * Reads ADC and converts it to a voltage.
 *
 * \returns
 *     A decimal representation of the voltage read.
 */
float readVoltage();

#endif /* PHOTOCELL_H_ */

/**
 * \}
 */
